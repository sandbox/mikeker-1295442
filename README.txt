Unlike many Git-based projects, the master branch does not contain any code. For
the Drupal 6.x version of this module, checkout the 6.x-1.x branch; for the
Drupal 7.x version, checkout the 7.x-1.x branch.

Eg:
  cd sites/all/modules/ftp_gallery
  git checkout 7.x-1.x
